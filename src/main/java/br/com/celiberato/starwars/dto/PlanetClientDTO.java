package br.com.celiberato.starwars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class PlanetClientDTO implements Serializable {

	static final long serialVersionUID = 1l;

	String name;
	String rotation_period;
	String orbital_period;
	String diameter;
	String climate;
	String gravity;
	String terrain;
	String surface_water;
	String population;
	List<String> residents;
	List<String> films;
	String created;
	String edited;
	String url;
}

