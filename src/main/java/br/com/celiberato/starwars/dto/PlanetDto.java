package br.com.celiberato.starwars.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PlanetDto {

    private Long id;
	private String name;
	private String climate;
	private String terrain;
	private Long quantity;

    public PlanetDto(String name, String climate, String terrain, Long quantity) {
        this.name = name;
        this.climate = climate;
        this.terrain = terrain;
        this.quantity = quantity;
    }
}
