package br.com.celiberato.starwars.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"br.com.celiberato.starwars.entity"})
@EnableJpaRepositories(basePackages = {"br.com.celiberato.starwars.repository"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
