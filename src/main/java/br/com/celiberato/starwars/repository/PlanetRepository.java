package br.com.celiberato.starwars.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.celiberato.starwars.entity.Planet;

@Repository
public interface PlanetRepository extends JpaRepository<Planet, Long> {

	Optional<Planet> findById(String id);

	List<Planet> findByName(String name);

}
