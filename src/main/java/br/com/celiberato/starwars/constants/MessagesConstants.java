package br.com.celiberato.starwars.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MessagesConstants {

	public static final String PLANET_INEXISTENTE = "Planeta não existe no banco de dados!";
	public static final String FILM_INEXISTENTE = "Filme não existe no banco de dados!";	
}

