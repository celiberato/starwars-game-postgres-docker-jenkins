package br.com.celiberato.starwars.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.starwars.constants.MessagesConstants;
import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;
import br.com.celiberato.starwars.exception.BusinessException;
import br.com.celiberato.starwars.repository.PlanetRepository;
import br.com.celiberato.starwars.response.PlanetClientResponse;
import br.com.celiberato.starwars.response.PlanetResponse;
import br.com.celiberato.starwars.util.EntityDtoUtil;

@Service
public class PlanetDBService {

	@Autowired
	private PlanetRepository repository;

	public List<Planet> getAll() {
		return this.repository.findAll();
	}

	public Planet getPlanetById(Long id) {
		return this.repository.findById(id).orElseThrow(IllegalArgumentException::new);
	}

	public List<Planet> getPlanetByName(String name) {
		return this.repository.findByName(name);
	}

	public Planet createPlanet(Planet planet) throws BusinessException {
		validate(planet);

		planet.setQuantity(100l);

		return repository.save(planet);

	}

	public Planet updatePlanet(Long id, PlanetDto planetDto) {
		planetDto.setId(id);
		return this.repository.save(EntityDtoUtil.toEntity(planetDto));
	}

	public void deletePlanet(Long id) throws BusinessException {
		if (!repository.existsById(id)) {
			throw new BusinessException(MessagesConstants.PLANET_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		this.repository.deleteById(id);
	}

	public void validate(Planet planet) throws BusinessException {

		if (!ObjectUtils.isEmpty(planet.getId()) && !repository.existsById(planet.getId())) {
			throw new BusinessException(MessagesConstants.PLANET_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

	}


	public boolean existsById(Long id) {
		return repository.findById(id).isPresent();
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}

}
