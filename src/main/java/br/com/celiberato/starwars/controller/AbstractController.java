package br.com.celiberato.starwars.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.AbstractDocument;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;
import br.com.celiberato.starwars.util.EntityDtoUtil;

public abstract class AbstractController {

	@Autowired
	protected ModelMapper modelMapper;


    protected <D, T> D convertEntityToDTO(final T model, final Class<D> dtoClass) {
		return model != null ? modelMapper.map(model, dtoClass) : null;
	}

	protected <D, E extends AbstractDocument> E convertToDocument(final D dto, final Class<E> entityClass) {
		return modelMapper.map(dto, entityClass);
	}


	protected <D, T> List<D> convertToDTO(final List<T> models, final Class<D> dtoClass) {
		List<D> dtos = new ArrayList<>();
		for (T model : models) {
			dtos.add(modelMapper.map(model, dtoClass));
		}

		return dtos;
	}

	protected <D, T> Page<D> convertToDTO(final Page<T> page, final Class<D> dtoClass) {
		return new PageImpl<>(convertToDTO(page.getContent(), dtoClass), page.getPageable(), page.getTotalElements());
	}

	protected <D, T> D convertToDTO(final T model, final Class<D> dtoClass) {
		return model != null ? modelMapper.map(model, dtoClass) : null;
	}

    
}
