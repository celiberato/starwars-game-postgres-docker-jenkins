package br.com.celiberato.starwars.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;
import br.com.celiberato.starwars.exception.BusinessException;
import br.com.celiberato.starwars.service.PlanetDBService;

@RestController
@RequestMapping("/db/planets")
public class PlanetDBController extends AbstractController {

    @Autowired
    private PlanetDBService service;
   
    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PlanetDto> all(){
        return convertToDTO(this.service.getAll(), PlanetDto.class);
    }

    @GetMapping("/name/{name}")
    public List<PlanetDto> getByName(@PathVariable("name") String name){
    	return convertToDTO(this.service.getPlanetByName(name), PlanetDto.class);
    }

    @GetMapping("{id}")
    public PlanetDto getPlanetById(@PathVariable Long id){
        return convertToDTO(this.service.getPlanetById(id), PlanetDto.class);
    }

    @PostMapping("/create")
    public PlanetDto createPlanet(@RequestBody PlanetDto planetDto) throws BusinessException{
    	return convertToDTO(service.createPlanet(convertToDTO(planetDto, Planet.class)), PlanetDto.class);
    }
    
    
    @PutMapping("{id}")
    public PlanetDto updatePlanet(@PathVariable Long id, @RequestBody PlanetDto planetDto){
       return convertToDTO(this.service.updatePlanet(id, planetDto), PlanetDto.class);
    }

    @DeleteMapping("{id}")
    public void deleteProduct(@PathVariable Long id) throws BusinessException {
    	this.service.deletePlanet(id);
    }
}
