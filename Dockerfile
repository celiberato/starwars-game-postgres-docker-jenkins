FROM openjdk:8-jdk-alpine

COPY target/starwars-game-postgres-docker-jenkins-0.0.1-SNAPSHOT.jar starwars.jar

EXPOSE 8091
 
ENTRYPOINT ["java","-jar","starwars.jar"]
