Checkout do Game StarWars!
- https://gitlab.com/celiberato/starwars-game.git

Como testar a API via POSTMAN?

Instale o software Postman:
- https://www.postman.com/downloads/

Importe o arquivo exportado do postman, no seguinte link:
- https://gitlab.com/celiberato/starwars-game/-/blob/master/postman/starwars.postman_collection.json

Como executar teste untários:

Instale a última versão do eclipse spring tools suite 4
- https://spring.io/tools

Clone o projeto para uma pasta do disco
- https://gitlab.com/celiberato/starwars-game.git


Execute o maven update do projeto

Execute o maven install na interface

--------------

https://tecadmin.net/install-oracle-java-8-ubuntu-via-ppa/

instalar docker:
https://phoenixnap.com/kb/how-to-install-docker-on-ubuntu-18-04

https://docs.docker.com/compose/install/

https://docs.sonarqube.org/latest/setup/get-started-2-minutes/
admin/123456

token sonar/jenkins: 7e3c61b206c8d90d7c18e87311754cf5f4f2f4be


JENKINS:
http://192.168.33.10:8080/

sudo docker-compose down
sudo docker stop pg-starwars || true
sudo docker rm pg-starwars || true

sudo docker-compose up -d

cd target
java -jar starwars-game-postgres-docker-jenkins-0.0.1-SNAPSHOT.jar
http://192.168.33.12:8080/

PLUGINS JENKINS : Sonar Qube Scanner / Sonar Quality Gate, instalar jdk11

sudo su -c 'echo 262144 > "/proc/sys/vm/max_map_count"'
http://192.168.33.12:9000/

PORT=8383
docker container ls --format="{{.ID}}\t{{.Ports}}" |\
grep ${PORT} |\
awk '{print $1}

SONAR:
mvn clean verify sonar:sonar -Dsonar.login=6e390d2e93fbfc8b0c31df9e36590de1b245c885

        <profile>
            <id>sonar</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <!-- Optional URL to server. Default value is http://localhost:9000 -->
                <sonar.host.url>
                  http://192.168.33.12:9000/
                </sonar.host.url>
            </properties>
        </profile>





- APLICAÇÃO SPRING BOOT:
http://34.71.148.241:8091/starwars/db/planets/all

- JENKINS:
http://34.71.148.241/

- POSTGRES DATABASE (ADM)
http://35.192.63.13:16543/

- NEXUS:
http://34.123.209.195/, http://cwisoftware.github.io/drops/usando-nexus-3-como-seu-repositrio-parte-1-artefatos-maven




POM.XML PARA NEXUS:
			<plugin>
			   <groupId>org.sonatype.plugins</groupId>
			   <artifactId>nexus-staging-maven-plugin</artifactId>
			   <version>1.5.1</version>
			   <executions>
			      <execution>
			         <id>default-deploy</id>
			         <phase>deploy</phase>
			         <goals>
			            <goal>deploy</goal>
			         </goals>
			      </execution>
			   </executions>
			   <configuration>
			      <serverId>nexus</serverId>
			      <nexusUrl>http://34.123.209.195/nexus/</nexusUrl>
			      <skipStaging>true</skipStaging>
			   </configuration>
			</plugin>

      <distributionManagement>
          <repository>
          <id>nexus</id>
          <name>Nexus Releases</name>
          <url>http://34.123.209.195/repository/nexus-releases/</url>
          </repository>
          <snapshotRepository>
          <id>nexus-snapshots</id>
          <url>http://34.123.209.195/repository/nexus-snapshots/</url>
          </snapshotRepository>
      </distributionManagement>


MAVEN SETING.XML:*

    <server>
        <id>nexus</id>
        <username>admin</username>
        <password>123456</password>
    </server>
    <server>
        <id>nexus-snapshots</id>
        <username>admin</username>
        <password>123456</password>
    </server>
    <server>
        <id>nexus-releases</id>
        <username>admin</username>
        <password>123456</password>
    </server>    



    <mirror>
        <id>central</id>
        <name>central</name>
        <url>http://34.123.209.195/repository/nexus-central/</url>
        <mirrorOf>*</mirrorOf>
    </mirror>     


    <activeProfiles>
        <activeProfile>nexus</activeProfile>
    </activeProfiles>  

    <profile>
        <id>nexus</id>
        <repositories>
          <repository>
            <id>central</id>
            <url>http://34.123.209.195/repository/nexus-group/</url>
            <releases>
              <enabled>true</enabled>
            </releases>
            <snapshots>
              <enabled>true</enabled>
            </snapshots>
          </repository>
        </repositories>
        <pluginRepositories>
          <pluginRepository>
            <id>central</id>
            <url>http://34.123.209.195/repository/nexus-group/</url>
            <releases>
              <enabled>true</enabled>
            </releases>
            <snapshots>
              <enabled>true</enabled>
            </snapshots>
          </pluginRepository>
        </pluginRepositories>
      </profile>



MINIKUBE:
- https://phoenixnap.com/kb/install-minikube-on-ubuntu
- https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy


- kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/head.yaml

- minikube start --driver=docker

- https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/

HIKARI POOL SPRING
https://github.com/brettwooldridge/HikariCP#configuration-knobs-baby
