# apt install dos2unix 
# dos2unix sobe-ambiente.sh



# install java 11
sudo apt update &&
sudo apt install openjdk-11-jdk &&
java -version


# install docker
sudo apt-get update &&
sudo apt-get remove docker docker-engine docker.io &&
sudo apt install docker.io &&
sudo systemctl start docker &&
sudo systemctl enable docker &&
docker --version

# install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &&
sudo chmod +x /usr/local/bin/docker-compose &&
[ -f /usr/bin/docker-compose ] && rm /usr/bin/docker-compose &&
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose &&
docker-compose --version


# install tomcat
cd tomcat &&
sudo docker build -t tomcat-server . &&
sudo docker run -it -p 8787:8080 -d -rm tomcat-server 


# install sonar
cd ../sonar &&
sudo docker-compose up -d


# install jenkins (8080)
cd ../jenkins 
#sudo docker network rm jenkins &&
#sudo docker network create jenkins && 
sudo docker-compose up -d

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

# node
udo apt-get install nodejs

 # install maven
 sudo apt-get install maven
  
  
  
  
  
# nexus ( Default credentials are: admin / 123456) http://192.168.33.12:808/
sudo apt-get update &&
cd ../nexus &&
sudo docker build --rm=true --tag=sonatype/nexus3 . &&
sudo curl -u admin:admin123 http://192.168.33.12:8081



#db-postgres
sudo docker-compose build
sudo docker-compose up -d